#!/bin/sh -e
docker buildx build --pull --push --tag chenio/analyzesemver:1.1.0 --tag chenio/analyzesemver:1.1 --tag chenio/analyzesemver:1 --tag chenio/analyzesemver:latest --platform linux/386,linux/amd64,linux/arm/v6,linux/arm/v7,linux/arm64/v8,linux/ppc64le,linux/s390x .

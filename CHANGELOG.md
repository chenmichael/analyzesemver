# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.2.1](https://gitlab.com/chenmichael/analyzesemver/-/compare/v1.2.0...v1.2.1) (2023-05-30)


### Bug Fixes

* **ci:** removed bad link from release-cli ([684bcd6](https://gitlab.com/chenmichael/analyzesemver/-/commit/684bcd61186003078b384c8a815a4f4345f317af))

## [1.2.0](https://gitlab.com/chenmichael/analyzesemver/-/compare/v1.1.0...v1.2.0) (2023-05-30)


### Features

* added combined semver tags ([d64396e](https://gitlab.com/chenmichael/analyzesemver/-/commit/d64396e28f8290e81ec1701051511df9f5c8bafb))
* added short options for cli options ([5bcbcb0](https://gitlab.com/chenmichael/analyzesemver/-/commit/5bcbcb02ddafc3551fff4685dfb95c20f41cd5fd))

## 1.1.0 (2023-05-30)


### Features

* added Dockerfile and analyze script ([647d3a2](https://gitlab.com/chenmichael/analyzesemver/-/commit/647d3a2bd36474a9f501b97752799e1d835c202f))
* added no-prerelease option and fix CMD/Entrypoint ([e8450a7](https://gitlab.com/chenmichael/analyzesemver/-/commit/e8450a7da5ebd5630c3fb6353504855a54127468))

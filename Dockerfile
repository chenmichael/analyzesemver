FROM python:3-alpine
RUN pip install semver~=3.0

COPY --chmod=755 analyze /usr/local/bin/
CMD [ "/bin/sh" ]
